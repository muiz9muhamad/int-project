@extends('layouts.app')
@section('title')
    Product | Edit
@endsection

@section('content')

<div class="card">
    <div class="body">
        <div class="card-inside-title">Edit Product</div>
        <div class="row clearfix">
            <div class="col-sm-12">
                <form action=" {{ route('product.update', $product->id) }} " method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <div class="form-line">
                        <input type="text" name="name" id="name" placeholder="Name"  class="form-control" value="{{ $product->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-line">
                            <label for="file">Upload Image</label>
                            <div class="fallback">
                                <input name="image" type="file" multiple />
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <div class="form-line">
                        <textarea class="form-control" name="image" id="image" rows="2" placeholder="Image">{{ $product->image }}</textarea>
                        </div>
                    </div> --}}
                    <br>
                    <div class="form-group">
                        <select class="form-control" name="cat_id" id="cat-id">
                            <option disabled selected>-- Categories --</option>
                            @foreach ($category as $item)
                                <option {{ $product->product_category_id == $item->id ? "selected" : "" }} value="{{ $item->id }}"> {{ $item->name }} </option>    
                            @endforeach
                        </select>
                        <hr>
                    </div><br>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea class="form-control" name="desc" id="desc" rows="2" placeholder="Description"> {{ $product->desc }} </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="amount" id="amount" placeholder="Amount"  class="form-control" value=" {{ $product->amount }} ">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection