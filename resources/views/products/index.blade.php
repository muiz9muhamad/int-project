@extends('layouts.app')
@section('title')
    Product
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Products</h2>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <div class="card-inside-title">Form Product</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('product.store') }} " method="post" id="simpanData" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="name" id="name" placeholder="Name"  class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="file">Upload Image</label>
                                <div class="fallback">
                                    <input name="imagessss" type="file" multiple />
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <div class="form-line">
                                <textarea class="form-control" name="image" id="image" rows="2" placeholder="Image"></textarea>
                            </div>
                        </div> --}}
                        <br>
                        <div class="form-group">
                            <select class="form-control show-tick" name="cat_id" id="cat-id">
                                <option disabled selected>-- Categories --</option>
                                @foreach ($category as $item)
                                    <option value="{{ $item->id }}"> {{ $item->name }} </option>    
                                @endforeach
                            </select>
                            <hr>
                        </div><br>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea class="form-control" name="desc" id="desc" rows="2" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="amount" id="amount" placeholder="Price"  class="form-control">
                            </div>
                        </div>
                        <button class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="body">
            <div class="card-inside-title">Table Product</div>
            <div class="table-responsive js-sweetalert">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Image</td>
                            <td>Category</td>
                            <td>Description</td>
                            <td>Price</td>
                            <td>Edit</td>
                            <td>Hapus</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Image</td>
                            <td>Category</td>
                            <td>Description</td>
                            <td>Price</td>
                            <td>Edit</td>
                            <td>Hapus</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($product as $n => $prods)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ $prods->name }}</td>
                                <td><img src="{{ $prods->image }}" height="80px" width="auto" alt="gambar" class="ssimg-thumbnail"></td>
                                <td>{{ $prods->products['name'] }}</td>
                                <td>{{ $prods->desc }}</td>
                                <td>{{ $prods->amount }}</td>
                                <td>
                                    <a href="{{ route('product.edit', $prods->id) }}">
                                        <button class="btn btn-success">Edit</button>
                                    </a>
                                </td>
                                <td>
                                    <form id="delete-data-{{ $prods->id }}" action=" {{ route('product.destroy', $prods->id) }} " method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger waves-effect" onclick="deleteNih({{ $prods->id }})">Delete</button>
                                    </form>
                                    
                                </td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection