@extends('layouts.app')
@section('title')
    Category | Edit
@endsection

@section('content')
    
    <div class="card">
        <div class="header">
            <h2>Edit Category</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('category.update', $category->id )}} " method="post" autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $category->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="3" name="desc" id="desc" class="form-control no-resize" placeholder="Descriptiom" >{{ $category->desc }}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection