@extends('layouts.app')
@section('title')
    Category | Show
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>{{ $category->name}} <small>{{ $category->desc }}</small></h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card-inside-title">Items</div>
                        @foreach ($category->productCat as $n => $prods)
                            <p>{{ $n+1 }}. {{ $prods->name }} </p>    
                        @endforeach
                </div>
            </div>
            <a href=" {{ route('category.index')}} "><button class="btn btn-warning">Back</button></a>
        </div>
    </div>
@endsection