<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($category as $n => $cats)
            <tr>
                <td>{{$n+1}}</td>
                <td>{{ $cats->name }}</td>
                <td>{{ $cats->desc }}</td>
                <td><a href=" {{ route('category.edit', $cats->id )}} ">Edit</a></td>
                <td><a href=" {{ route('category.show', $cats->id )}} ">Show</a></td>
                <td>
                    <form id="delete-data-{{ $cats->id }}" action=" {{ route('category.destroy', $cats->id) }} " method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger" onclick="deleteNih({{ $cats->id }})">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('script')
<script>
        function deleteNih(id){
            event.preventDefault();
            var form = document.querySelector('#delete-data-'+id);
    
            swal({
                title: "Apa anda yakin?",
                text: "Anda tidak dapat mengembalikannya lagi!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
    
                form.submit();
    
            });
    
        }

        
</script>
<script>
    
   
</script>
@endsection