@extends('layouts.app')
@section('title')
    Category
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Categories</h2>
        </div>
        <div class="body">
            <div class="card-inside-title">Form Category</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('category.store') }} " id="simpanData" method="post" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="name" id="name" placeholder="Name"  class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="3" name="desc" id="desc" class="form-control no-resize" placeholder="Descriptiom"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="body">
            <div class="card-inside-title">Table Category</div>
            @include('category.table')
        </div>
    </div>
@endsection
