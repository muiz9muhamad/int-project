@extends('layouts.app')
@section('title')
    Member | Edit
@endsection

@section('content')

<div class="card">
    <div class="body">
        <div class="card-inside-title">Edit Product</div>
        <div class="row clearfix">
            <div class="col-sm-12">
                <form action="{{ route('members.update', $members->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <select class="form-control show-tick" name="mem_cat" id="mem_cat">
                            <option disabled selected>-- Categories --</option>
                            @foreach ($memcat as $item)
                                <option {{ $members->member_category_id == $item->id ? "selected" : "" }} value="{{ $item->id }}"> {{ $item->name }} </option>    
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="fullname" id="fullname" placeholder="Full Name" class="form-control" value=" {{ $members->full_name}} ">
                        </div>
                    </div>
                    <div class="input-group date" id="bs_datepicker_component_container">
                        <label for="dob">Tanggal Lahir</label>
                            <div class="form-line">
                                <input type="text" name="dob" id="dob" class="form-control" value="{{ $members->dob }}">
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="address" id="address" placeholder="Alamat" class="form-control" value=" {{ $members->address}} ">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="demo-radio-button">
                            <input name="gender" type="radio" id="male" value="male" {{ $members->gender == 'male' ? 'checked' : ''}} class="with-gap" checked />
                            <label for="male">Laki-laki</label>
                            <input name="gender" type="radio" id="female" value="female" {{ $members->gender == 'female' ? 'checked' : ''}} class="with-gap" />
                            <label for="female">Perempuan</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection