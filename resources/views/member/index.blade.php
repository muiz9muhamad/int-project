@extends('layouts.app')
@section('title')
    Member
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Member</h2>
        </div>
        <div class="body">
            <div class="card-inside-title">Form Member</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('members.store')}} " id="simpanData" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <select class="form-control show-tick" name="mem_cat" id="mem_cat">
                                <option disabled selected>-- Categories --</option>
                                @foreach ($memcat as $item)
                                    <option value="{{ $item->id }}"> {{ $item->name }} </option>    
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="fullname" id="fullname" placeholder="Full Name" class="form-control">
                            </div>
                        </div>
                        <div class="input-group date" id="bs_datepicker_component_container">
                            <label for="dob">Tanggal Lahir</label>
                                <div class="form-line">
                                    <input type="date" name="dob" id="dob" class="form-control" placeholder="Please choose a date...">
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="address" id="address" placeholder="Alamat" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="demo-radio-button">
                                <input name="gender" type="radio" id="male" value="male" class="with-gap" checked />
                                <label for="male">Laki-laki</label>
                                <input name="gender" type="radio" id="female" value="female" class="with-gap" />
                                <label for="female">Perempuan</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="body">
            <div class="card-inside-title">Table Member</div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <td>NO</td>
                            <td>Member Category</td>
                            <td>Full Name</td>
                            <td>Tanggal Lahir</td>
                            <td>Alamat</td>
                            <td>Jenis Kelamin</td>
                            <td>Barcode</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>NO</td>
                            <td>Member Category</td>
                            <td>Full Name</td>
                            <td>Tanggal Lahir</td>
                            <td>Alamat</td>
                            <td>Jenis Kelamin</td>
                            <td>Barcode</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($members as $n => $member)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ $member->category['name'] }}</td>
                                <td>{{ $member->full_name }}</td>
                                <td>{{ $member->dob }}</td>
                                <td>{{ $member->address }}</td>
                                <td>{{ $member->gender }}</td>
                                <td>{{ $member->barcode }}</td>
                                <td>
                                    <a href=" {{ route('members.edit', $member->id) }}"><button class="btn btn-success">Edit</button></a>
                                </td>
                                <td>
                                    <form id="delete-data-{{ $member->id }}" action=" {{ route('members.destroy', $member->id)}} " method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" onclick="deleteNih({{ $member->id }})">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

{{-- @section('script')
<script>
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#delete-data-'+id);

        swal({
            title: "Apa anda yakin?",
            text: "Anda tidak dapat mengembalikannya lagi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(){

            form.submit();

        })

    }
</script>
@endsection

@section('script2')
    <script type="text/javascript" class="init">
        
    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);

            swal({
                        title: 'Success',
                        text:"Berhasil Menambahkan Data",
                        type:"success",
                        timer: 1000,
                        showConfirmButton: false
                    }, function() {
                        form.submit();
                    });

				
            })
        } );
    </script>
@endsection --}}