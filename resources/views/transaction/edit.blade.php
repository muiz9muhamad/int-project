@extends('layouts.app')
@section('title')
    Transaction | Edit
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Transaction</h2>
        </div>
        <div class="body">
            <div class="card-inside-title">Form Transaction</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('transactions.update', $trans->id)}} " id="simpanData" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control show-tick" name="product_id" id="product_id">
                                    <option disabled selected>-- Produk --</option>
                                    @foreach ($products as $product)
                                        <option {{ $trans->product_id == $product->id ? "selected" : "" }} value="{{ $product->id }}"> {{ $product->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <input type="text" name="quantity" id="quantity" placeholder="Quantity" class="form-control" value="{{ $trans->quantity }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection