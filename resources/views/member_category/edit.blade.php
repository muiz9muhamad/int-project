@extends('layouts.app')
@section('title')
    Member Category | Edit
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Transaction</h2>
        </div>
        <div class="body">
            <div class="card-inside-title">Form Transaction</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('member_categories.update', $memberCategory->id)}} " id="simpanData" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="form-line">
                            <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ $memberCategory->name }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection