@extends('layouts.app')
@section('title')
    Member Category
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Member Category</h2>
        </div>
        <div class="body">
            <div class="card-inside-title">Form Member Category</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action=" {{ route('member_categories.store')}} " id="simpanData" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="name" id="name" placeholder="Name" class="form-control">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="body">
            <div class="card-inside-title">Table Member Category</div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <td>NO</td>
                            <td>Name</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>NO</td>
                            <td>Name</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($memcats as $n => $memcat)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ $memcat->name }}</td>
                                <td>
                                    <a href=" {{ route('member_categories.edit', $memcat->id) }}"><button class="btn btn-success">Edit</button></a>
                                </td>
                                <td>
                                    <form id="delete-data-{{ $memcat->id }}" action=" {{ route('member_categories.destroy', $memcat->id)}} " method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" onclick="deleteNih({{ $memcat->id }})">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

{{-- @section('script')
<script>
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#delete-data-'+id);

        swal({
            title: "Apa anda yakin?",
            text: "Anda tidak dapat mengembalikannya lagi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(){

            form.submit();

        })

    }
</script>
@endsection --}}

{{-- @section('script2')
    <script type="text/javascript" class="init">
        
    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);

            swal({
                        title: 'Success',
                        text:"Berhasil Menambahkan Data",
                        type:"success",
                        timer: 1000,
                        showConfirmButton: false
                    }, function() {
                        form.submit();
                    });

				
            })
        } );
    </script>
@endsection --}}