@extends('layouts.app')
@section('title')
    Promo
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Promo</h2>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <div class="card-inside-title">Form Promo</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <form action="" method="post" id="simpanData" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="name" id="name" placeholder="Name"  class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control show-tick" name="product_id" id="product_id">
                                    <option disabled selected>-- Produk --</option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}"> {{ $product->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="discount" id="discount" placeholder="Discount"  class="form-control">
                            </div>
                        </div>
                        <button class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="body">
            <div class="card-inside-title">Table Promo</div>
            <div class="table-responsive js-sweetalert">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Produk</td>
                            <td>Diskon</td>
                            <td>Edit</td>
                            <td>Hapus</td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Produk</td>
                            <td>Diskon</td>
                            <td>Edit</td>
                            <td>Hapus</td>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($promos as $n => $promo)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ $promo->name }}</td>
                                <td>{{ $promo->prods['name'] }}</td>
                                <td>{{ $promo->discount }}</td>
                                <td>
                                    <a href="{{ route('promos.edit', $promo->id)}}">
                                    <button class="btn btn-success waves-effect">Edit</button>
                                    </a>
                                </td>
                                <td>
                                    <form id="delete-data-{{ $promo->id }}" action="{{ route('promos.destroy', $promo->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger waves-effect" onclick="deleteNih({{ $promo->id }})" >Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

{{-- @section('script')

<script>
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#delete-data-'+id);

        swal({
            title: "Apa anda yakin?",
            text: "Anda tidak dapat mengembalikannya lagi!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(){

            form.submit();

        })

    }

</script>
    
@endsection --}}