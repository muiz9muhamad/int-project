@extends('layouts.app')
@section('title')
    Promo | Edit
@endsection

@section('content')
    <div class="card">
        <div class="header">
            <h2>Promo</h2>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <div class="card-inside-title">Edit Form</div>
            <div class="row clearfix">
                <div class="col-sm-12">
                <form action=" {{ route('promos.update', $promos->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="name" id="name" placeholder="Name" value="{{ $promos->name }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control show-tick" name="product_id" id="product_id">
                                    <option disabled selected>-- Produk --</option>
                                    @foreach ($products as $product)
                                        <option {{ $promos->product_id == $product->id ? "selected" : "" }} value="{{ $product->id }}"> {{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" name="discount" id="discount" placeholder="Discount" value="{{ $promos->discount }}"  class="form-control">
                            </div>
                        </div>
                        <button class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    
@endsection