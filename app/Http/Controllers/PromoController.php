<?php

namespace App\Http\Controllers;

use App\Product;
use App\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $promos = Promo::with('prods')->get();

        // return response()->json($promos);
        return view('promo.index', compact('promos', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'product_id' => 'required',
            'discount' => 'required',
        ]);

        $promo = new Promo();
        $promo->name = $request->name;
        $promo->product_id = $request->product_id;
        $promo->discount = $request->discount;
        $promo->save();

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function show(Promo $promo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function edit(Promo $promo)
    {
        $promos = $promo;
        $products = Product::all();
        return view('promo.edit', compact('promos', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promo $promo)
    {
        $request->validate([
            'name' => 'required',
            'product_id' => 'required',
            'discount' => 'required',
        ]);

        $promos = $promo;
        $promos->name = $request->name;
        $promos->product_id = $request->product_id;
        $promos->discount = $request->discount;
        $promos->update();

        // return response()->json($promos);
        return redirect('promos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promo $promo)
    {
        // $promos = $promo::findOrFail($id);
        $promo->delete();

        return redirect()->route('promos.index');

    }
}
