<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Product;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $trans = Transaction::with('prods')->get();

        // return response()->json($trans);
        return view('transaction.index', compact('products', 'trans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
        ]);
        
        
        //trx number generator
        $rmdash = str_replace("-","",Carbon::now()->toDateTimeString());
        $rmcolon = str_replace(":","",$rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $trx_number = 'TRX'.$rmspace;

        $init = Product::where('id', $request->product_id)->get()->first()->amount;
        
        // return response()->json($init);

        $total = $request->quantity * $init;

        $transaction = new Transaction();
        $transaction->trx_number = $trx_number;
        $transaction->product_id = $request->product_id;
        $transaction->quantity = $request->quantity;
        $transaction->discount = 0;
        $transaction->total = $total;
        $transaction->save();

        // dd($total);
        return redirect('transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $trans = $transaction;
        $products = Product::all();

        return view('transaction.edit', compact('trans','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
        ]);
        
        $init = Product::where('id', $request->product_id)->get()->first()->amount;
        $total = $request->quantity * $init;

        $transaction->product_id = $request->product_id;
        $transaction->quantity = $request->quantity;
        $transaction->discount = 0;
        $transaction->total = $total;
        $transaction->update();

        // dd($total);
        return redirect('transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return redirect('transactions');
    }
}
