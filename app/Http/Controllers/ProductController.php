<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        $product = Product::with('products')->get();

        return view('products.index', compact('product','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'imagessss' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'image' => 'required',
            'cat_id' => 'required',
            'desc' => 'required',
            'amount' => 'required'
        ]);

        // return response()->json($request);

        if ($request->has('active')) {
            $active = 1;
         } else {
             $active = 0;
         }

        $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->imagessss->getClientOriginalExtension();

        if(!$request->imagessss->move(storage_path('app/public/produk'), $fileName)){
            return array('error' => 'Gagal upload foto');
        } else {
            $product = new Product();
            $product->name = $request->name;
            $product->image = "storage/produk/".$fileName;;
            $product->product_category_id = $request->cat_id;
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            $product->save();
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = Category::all();
        return view('products.edit', compact('product', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productPict = Product::where("id","=",$id)->get()->first()->image;

        // return response()->json($productPict);

        if (!$request->image) {
            
            $request->validate([
                'name' => 'required',
                'cat_id' => 'required',
                'desc' => 'required',
                'amount' => 'required'
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'cat_id' => 'required',
                'desc' => 'required',
                'amount' => 'required'
            ]);

            $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
        }
        
        if ($request->has('active')) {
            $active = 1;
        } else {
            $active = 0;
        }
        

        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->product_category_id = $request->cat_id;
        $product->desc = $request->desc;
        $product->amount = $request->amount;
        $product->image = $request->image;
        if($request->hasFile('image')){
            if (is_file($product->image)){
                try{
                    unlink($productPict);
                } catch(\Exception $e){

                }
            }
            $request->image->move(storage_path('app/public/produk'), $fileName);
            $product->image = "storage/produk/".$fileName;
        } else {
            $product->image = $productPict;
        }
        $product->save();

        return redirect('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('product');
    }
}
