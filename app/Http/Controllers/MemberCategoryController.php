<?php

namespace App\Http\Controllers;

use App\MemberCategory;
use Illuminate\Http\Request;

class MemberCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memcats = MemberCategory::all();

        return view('member_category.index', compact('memcats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $mem = new MemberCategory();
        $mem->name = $request->name;
        $mem->save();

        return redirect('member_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MemberCategory $memberCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MemberCategory $memberCategory)
    {
        return view('member_category.edit', compact('memberCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberCategory $memberCategory)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $mem = $memberCategory;
        $mem->name = $request->name;
        $mem->update();

        return redirect('member_categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberCategory $memberCategory)
    {
        $memberCategory->delete();

        return redirect('member_categories');
    }
}
