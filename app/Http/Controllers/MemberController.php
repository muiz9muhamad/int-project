<?php

namespace App\Http\Controllers;

use App\Member;
use App\MemberCategory;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::with('category')->get();
        $memcat = MemberCategory::all();

        return view('member.index', compact('members', 'memcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'mem_cat' => 'required',
            'fullname' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        // $radio = $request->get('radio_button', 0);

        // Barcode
        $rmdash = str_replace("-","",$request->dob);
        $rmcolon = str_replace(":","",$rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $barcode = 'BC'.$rmspace;

        $member = new Member();
        $member->member_category_id = $request->mem_cat;
        $member->full_name = $request->fullname;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->barcode = $barcode;
        $member->save();

        return redirect('members');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $members = $member;
        $memcat = MemberCategory::all();

        return view('member.edit', compact('members','memcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $request->validate([
            'mem_cat' => 'required',
            'fullname' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        $member->member_category_id = $request->mem_cat;
        $member->full_name = $request->fullname;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->update();

        // return response()->json($member);

        return redirect('members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();

        return redirect('members');
    }
}
