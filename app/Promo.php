<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    protected $guarded = [];
    use SoftDeletes;

    public function prods(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
