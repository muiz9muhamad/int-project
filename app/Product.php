<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public function products(){
        return $this->belongsTo(Category::class, 'product_category_id');
    }

    public function promo(){
        return $this->hasMany(Promo::class, 'product_id');
    }
}
